import requests
import vk_api
from vk_api.longpoll import VkLongPoll, VkEventType
from loguru import logger
import random


class LongPollIterator:
    def __init__(self, _session):
        self.longpoll = VkLongPoll(_session)

    def __iter__(self):
        for event in self.longpoll.listen():
            logger.debug(event.type)
            yield event


class LongPollBot:
    HELP_MESSAGE = '''Отправьте id пользователя'''

    def __init__(self, _token, _spider):
        self.vk_session = vk_api.VkApi(token=_token)
        self.vk = self.vk_session.get_api()
        self.eventIterator = LongPollIterator(self.vk_session)
        self.spider = _spider

    def loop(self):
        for event in self.eventIterator:
            self.handle(event)

    def handle(self, event):
        result = None

        # NEW MESSAGE
        if event.type == VkEventType.MESSAGE_NEW:

            # Private message (not group)
            if event.to_me:

                # With text
                if event.text:
                    result = self.simple_message_process(event)

        if result:
            logger.info(result)
            self.vk.messages.send(**result)

    def simple_message_process(self, event):
        photo = None
        if 'https://vk.com/' not in event.text:
            message = 'Нужна ссылка на профиль'
        else:
            message, photo = self.spider.get_all_info(event.text)

        return {
            'user_id': event.user_id,
            'message': message,
            'reply_to': event.message_id,
            'attachment': photo,
            'random_id': random.randint(0, 0xffffffff)
        }
