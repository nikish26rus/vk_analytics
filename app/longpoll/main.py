import configparser
import os.path

from frontend.LongPollBot import LongPollBot
from backend.source import VK_spider

from loguru import logger

if __name__ == '__main__':
    CONFIG_PATH = 'config.ini'

    config = configparser.ConfigParser()
    config.read(CONFIG_PATH)

    try:
        vk_token = config.get('frontend', 'token')

        login = config.get('vk_user', 'login')
        password = config.get('vk_user', 'pass')

        api_id = config.get('vk_user', 'app_id')
        protected_key = config.get('vk_user', 'protected_key')
        service_key = config.get('vk_user', 'service_key')
    except (configparser.NoOptionError, configparser.NoSectionError) as e:
        logger.error(f'Проверьте правильность заполнения файла {os.path.abspath("config.ini")}')
        logger.error(e)
        exit(666)

    spider = VK_spider(login, password, api_id, protected_key, service_key)
    bot = LongPollBot(vk_token, spider)
    bot.loop()
