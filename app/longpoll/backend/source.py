from pprint import pprint
import vk
from time import sleep
from datetime import datetime
import requests
import re
from loguru import logger


class VK_spider:
    def __init__(self, _login, _password, _app_id, _protected_key, _service_key):
        user_agent = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64)\
                 AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.135 S ' \
                     '' \
                     'afari/537.36 Edge/12.246'

        # Логин пользователя от имени которого вы хотите выполнять действия
        user_login = _login

        # Пароль
        user_password = _password

        # id приложения
        app_id = _app_id

        # session = vk.AuthSession(app_id=app_id, user_login=user_login, user_password=user_password)

        protected_key = _protected_key
        service_key = _service_key

        session = vk.Session(access_token=service_key)

        self.vk_api = vk.API(session)
        self.version_API = '5.126'

    def get_user_id(self, url):
        headers_get = {
            'Host': 'regvk.com',
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0',
            'Accept': 'text/html, application/xhtml+xml, application/xml;q=0.9,image/webp,*/*;q=0.8',
            'Accept-Language': 'ru-RU,ru;q=0.8,en-US;q=0.5,en;q=0.3',
            'Accept-Encoding': 'gzip, deflate, br',
            'Upgrade-Insecure-Requests': '1',
            'DNT': '1',
            'Sec-GPC': '1',
            'TE': 'Trailers',
        }
        get_result = requests.get('https://regvk.com/id/', headers=headers_get)
        cookie = re.findall(r'cookie="(.*?);', str(get_result.content))[0]

        headers_post = {
            'Host': 'regvk.com',
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0',
            'Accept': 'text/html, application/xhtml+xml, application/xml;q=0.9,image/webp,*/*;q=0.8',
            'Accept-Language': 'ru-RU,ru;q=0.8,en-US;q=0.5,en;q=0.3',
            'Accept-Encoding': 'gzip, deflate, br',
            'Content-Type': 'application/x-www-form-urlencoded',
            'Content-Length': '115',
            'Origin': 'https://regvk.com',
            'Connection': 'keep-alive',
            'Referer': 'https://regvk.com/id/',
            'cookie': f'{cookie}',
            'Upgrade-Insecure-Requests': '1',
            'DNT': '1',
            'Sec-GPC': '1'
        }
        post_result = requests.post(url='https://regvk.com/id/',
                                    data={'link': url, 'button': 'Определить+ID'}, headers=headers_post)
        real_id = re.findall(r'ID пользователя: (\d+)', str(post_result.text))[0]
        return real_id

    def check_id(self, url):
        valid_id = re.match('id\d+', url[15:])

        if valid_id:
            return url[17:]
        else:
            return self.get_user_id(url)

    def get_user_info(self, user_id):
        fields_for_user_get = 'about, bdate, city, connections, contacts, counters,' \
                              ' country, domain, exports, home_town, photo_max_orig, site'
        try:
            logger.debug(f'VK_API Request users.get {user_id}')
            user_info = self.vk_api.users.get(v=self.version_API, user_id=user_id, fields=fields_for_user_get)[0]
        except Exception as error:
            print(error)
            user_info = []

        # pprint(user_info)

        res = {
            'first_name': user_info['first_name'],
            'last_name': user_info['last_name'],
            'domain': user_info['domain'],
            'id': user_info['id'],
            'photo_max_orig': user_info['photo_max_orig'],
            'is_closed': user_info['is_closed'],
            'bdate': user_info['bdate'] if 'bdate' in user_info else None,
            'city': user_info['city']['title'] if 'city' in user_info else None,
            'country': user_info['country']['title'] if 'country' in user_info else None,
            'posts_count': user_info['counters']['posts'] if 'posts' in user_info['counters'] else None
        }
        return res

    def get_wall_info(self):
        try:
            wall_info = self.vk_api.wall.get(v=self.version_API, owner_id=self.user_id)
        except Exception as error:
            print(error)
            wall_info = []
        sleep(1)

        # pprint(wall_info['items'])

    def get_post_info(self, post_id):
        try:
            post_info = self.vk_api.wall.getComments(v=self.version_API,
                                                     owner_id=self.user_id, post_id=post_id, need_likes=1)
        except Exception as error:
            print(error)
            post_info = []
        sleep(1)
        # pprint(post_info)

    def get_friends(self, user_id):
        fields = 'bdate, city, country'
        try:
            logger.debug(f'VK_API Request friends.get {user_id}')
            friends = self.vk_api.friends.get(v=self.version_API, user_id=user_id, fields=fields)
        except Exception as error:
            print(error)
            friends = []

        estimated_age = self.get_estimated_age(friends)
        estimated_city = self.get_estimated_city(friends)
        estimated_country = self.get_estimated_country(friends)

        res = {
            'estimated_age': estimated_age,
            'estimated_city': estimated_city,
            'estimated_country': estimated_country
        }
        return res

    @staticmethod
    def get_estimated_age(friends):
        now = datetime.now()
        friends_age = []
        for friend in friends['items']:
            if 'bdate' in friend:
                bdate_mas = friend['bdate'].split('.')
                if len(bdate_mas) == 3:
                    bdate = datetime.strptime(friend['bdate'], "%d.%m.%Y")
                    delta = now - bdate
                    friends_age.append(delta.days // 365)

        age_stat = {age: friends_age.count(age) for age in friends_age}
        list_ages = list(age_stat.items())
        list_ages.sort(key=lambda i: i[1], reverse=True)
        return list_ages[0][0]

    @staticmethod
    def get_estimated_city(friends):
        friends_cities = []
        for friend in friends['items']:
            if 'city' in friend:
                friends_cities.append(friend['city']['title'])

        cities_stat = {city: friends_cities.count(city) * 100 // friends_cities.__len__()
                       for city in friends_cities}
        list_cities = list(cities_stat.items())
        list_cities.sort(key=lambda i: i[1], reverse=True)

        result = ''
        for city in list_cities[:3]:
            if city[1] == 0:
                continue
            result += f'{city[0]}—{city[1]}% '

        return result

    @staticmethod
    def get_estimated_country(friends):
        friends_countries = []
        for friend in friends['items']:
            if 'country' in friend:
                friends_countries.append(friend['country']['title'])

        countries_stat = {country: friends_countries.count(country) * 100 // friends_countries.__len__()
                          for country in friends_countries}

        list_countries = list(countries_stat.items())
        list_countries.sort(key=lambda i: i[1], reverse=True)

        result = ''
        for country in list_countries[:3]:
            if country[1] == 0:
                continue
            result += f'{country[0]}—{country[1]}% '

        return result

    def get_all_info(self, user_url):
        user_id = self.check_id(user_url)
        result_str = ''
        user_info = self.get_user_info(user_id)
        photo = user_info["photo_max_orig"]
        # print(photo)

        if user_info['is_closed'] is True:
            result_str += f'Закрытый профиль\n'
        result_str += f'Имя пользователя: {user_info["first_name"]} {user_info["last_name"]}\n'
        result_str += f'ID: https://vk.com/id{user_info["id"]}\n'
        if user_info['domain'] != user_info['id']:
            result_str += f'Короткий адрес страницы: https://vk.com/{user_info["domain"]}\n'
        result_str += f'День рождения: {user_info["bdate"]}\n'
        if user_info['country']:
            result_str += f'Страна: {user_info["country"]}\n'
        if user_info['city']:
            result_str += f'Город: {user_info["city"]}\n'

        if user_info['is_closed'] is not True:
            friends_info = self.get_friends(user_id)
            result_str += f'Предполагаемый возраст: {friends_info["estimated_age"]}\n'
            result_str += f'Предполагаемые города: {friends_info["estimated_city"]}\n'
            result_str += f'Предполагаемые страны: {friends_info["estimated_country"]}\n'

        return result_str, photo


if __name__ == "__main__":
    spider = VK_spider()
    # spider.test()
    # spider.get_wall_info()
    # spider.get_friends(353057430)
    # spider.get_friends(290414996)
    # spider.get_friends(290414996)
    spider.get_all_info('https://vk.com/nikinax')
